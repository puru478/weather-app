//
//  ViewController.swift
//  Weather App
//
//  Created by Purushottam Chandra on 03/03/17.
//  Copyright © 2017 Kuliza-335. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var headerImageView: CustomImage!
    @IBOutlet weak var cityTextfield: UITextField!
    
    @IBOutlet weak var visualEffectView: UIVisualEffectView!
    @IBOutlet weak var quitButton: UIButton!
    @IBOutlet weak var noButotn: CustomButton!
    @IBOutlet weak var yesButton: CustomButton!
    @IBOutlet var customAlert: UIView!
    @IBOutlet weak var displayLabel: UILabel!
    
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    
    var weatherCondition : String!
    var temperature : Float!
    var humidity : Float!
    var effect : UIVisualEffect!
    
    var complete : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        yesButton.layer.cornerRadius = yesButton.frame.width / 2
        noButotn.layer.cornerRadius = noButotn.frame.width / 2
        customAlert.alpha = 0
        customAlert.center = self.view.center
        customAlert.layer.cornerRadius = 8
        displayLabel.alpha = 0
        temperatureLabel.alpha = 0
        humidityLabel.alpha = 0
        submitButton.isHidden = true
        
        effect = visualEffectView.effect
        visualEffectView.effect = nil
        
        cityTextfield.addTarget(self, action: #selector(showSubmitButton), for: .editingChanged)
        submitButton.addTarget(self, action: #selector(getData), for: .touchUpInside)
        yesButton.addTarget(self, action: #selector(quitApplication), for: .touchUpInside)
        noButotn.addTarget(self, action: #selector(dismissAlert), for: .touchUpInside)
        quitButton.addTarget(self, action: #selector(onClickQuitButton), for: .touchUpInside)
    }
    
    func showSubmitButton() {
        submitButton.isHidden = false
        if( complete == true) { self.complete = false }
        if(cityTextfield.text == "")
        {
            displayLabel.alpha = 0
            temperatureLabel.alpha = 0
            self.humidityLabel.alpha = 0
        }
    }
    
    func getData() {
        
        var cityName = cityTextfield.text
        
        if(cityName?.contains(" ") == true)
        {
            cityName = cityName?.replacingOccurrences(of: " ", with: "+")
        }
        
        let stringURL = "http://api.openweathermap.org/data/2.5/weather?q=\(cityName!),&appid=db3cb717a9bde6b3b0f50e2e2bb491b9"
        let url = URL(string: stringURL)
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if(error != nil)
            {
                print("ERROR: \(error)")
            }
            else
            {
                if let content = data {
                    do
                    {
                        let myJson = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        print("JSON DATA: \(myJson)")
                        
                        if let weather = myJson["weather"] as? NSArray
                        {
                            print(weather)
                            if let firstOBJ = weather.firstObject as? NSDictionary
                            {
                                if let desc = firstOBJ["description"]
                                {
//                                    print(desc)
                                    self.weatherCondition = desc as! String
//                                    print(self.weatherCondition)
                                }
                            }
                        }
                        
                        if let mainJSON = myJson["main"] as? NSDictionary
                        {
                            print(mainJSON)
                            if let firstOBJ = mainJSON["temp"] as? Float
                            {
//                                if let desc = firstOBJ["temperature"]
//                                {
                                    print(firstOBJ)
                                    self.temperature = firstOBJ
                                    print(self.temperature)
//                                }
                            }
                            
                            if let secondOBJ = mainJSON["pressure"] as? Float
                            {
                                print(secondOBJ)
                                self.humidity = secondOBJ
                                print(self.humidity)
                                //                                }
                            }

                        }
                        
                        
                        
                    }
                        
                    catch
                    {
                        print("Error again!")
                    }
                }
                
            }
        }
        task.resume()
        
        if(complete == false) {
            complete = true

        UIView.animate(withDuration: 0.3,
            animations: {
                self.headerImageView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI)).concatenating(CGAffineTransform(scaleX: 0.001, y: 0.001))
//                self.onClickSubmit()
                self.getData()

            },
            completion: {
                (success: Bool) in
                
                UIView.animate(withDuration: 0.3,
                    animations: {
                        self.onClickSubmit()

                        self.headerImageView.transform = CGAffineTransform.identity

                    })
                })
        }
    }
    
    func onClickSubmit()
    {
        if(temperature != nil)
        {
            self.temperatureLabel.alpha = 1
            self.temperatureLabel.text = weatherCondition
        
            self.displayLabel.alpha = 1
            self.displayLabel.text = "Temperature: \(round(kelvinTo(celsius: temperature!))) Celsius"
            
            self.humidityLabel.alpha = 1
            self.humidityLabel.text = "Humidity: \(round(humidity!))"
            
            if(cityTextfield.text == "Mumbai" || cityTextfield.text == "London")
            {  self.headerImageView.image = UIImage(named: "home")}

            else if (self.weatherCondition.contains("sky") || self.weatherCondition.contains("clear"))
            {   self.headerImageView.image = UIImage(named: "forest")}
            else if( self.weatherCondition.contains("rain") || self.weatherCondition.contains("drizzle") )
            { self.headerImageView.image = UIImage(named: "waterdrop")}
            else if( self.weatherCondition.contains("snow"))
            { self.headerImageView.image = UIImage(named: "snowman")}
            else if( self.weatherCondition.contains("sun"))
            { self.headerImageView.image = UIImage(named: "forestred")}
            else if( self.weatherCondition.contains("cloud"))
            { self.headerImageView.image = UIImage(named: "cloudy")}
            
        }
            
        else
        {
            self.displayLabel.alpha = 0
            self.displayLabel.text = ""
            
            self.temperatureLabel.alpha = 0
            self.temperatureLabel.text = ""
            
            self.humidityLabel.alpha = 0
            self.humidityLabel.text = ""
        }
    }
    
    func kelvinTo(celsius: Float) -> Float
    {
        let kelvin = (celsius - 273.15)
        return kelvin
    }
    
    func quitApplication()
    {
        exit(0)
    }
    
    func dismissAlert()
    {
        UIView.animate(withDuration: 0.3) {
            self.customAlert.alpha = 0
            self.customAlert.removeFromSuperview()
            self.quitButton.alpha = 1
            self.visualEffectView.effect = nil
            self.visualEffectView.alpha = 0
        }
    }
    
    func onClickQuitButton()
    {
        UIView.animate(withDuration: 0.3) {
            self.view.addSubview(self.customAlert)
            self.customAlert.alpha = 1
            self.quitButton.alpha = 0
            self.visualEffectView.effect = self.effect
            self.visualEffectView.alpha = 1
        }
        
    }
}
